
// Playing Cards Lab 
// Vincent Morrill (initial)

#include <iostream>
#include <conio.h>

using namespace std;


enum Suit {
	Hearts, Diamonds, Spades, Clubs
};

enum Rank {
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
	
	//Uncomment line below if Joker is not being used. Doing so will shift back to zero-based card ranks
	//Joker, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};


//Structure for a Card
struct Card {
	Suit suit;
	Rank number;
};

void PrintCard(Card card)
{
	cout << "The card suit is ";

	switch (card.suit)
	{
	case Hearts:
		cout << "Hearts";
		break;
	case Diamonds:
		cout << "Diamonds";
		break;
	case Spades:
		cout << "Spades";
		break;
	case Clubs:
		cout << "Clubs";
		break;
	}

	cout << ". The card rank is ";

	switch (card.number)
	{
	case 2:
		cout << "Two.";
		break;
	case 3:
		cout << "Three.";
		break;
	case 4:
		cout << "Four.";
		break;
	case 5:
		cout << "Five.";
		break;
	case 6:
		cout << "Six.";
		break;
	case 7:
		cout << "Seven.";
		break;
	case 8:
		cout << "Eight.";
		break;
	case 9:
		cout << "Nine.";
		break;
	case 10:
		cout << "Ten.";
		break;
	case 11:
		cout << "Jack.";
		break;
	case 12:
		cout << "Queen.";
		break;
	case 13:
		cout << "King.";
		break;
	case 14:
		cout << "Ace.";
		break;
	}

	cout << endl;
}

Card HighCard(Card card1, Card card2)
{
	if (card1.number > card2.number)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}

int main()
{
	Card firstCard;
	firstCard.suit = Hearts;
	firstCard.number = Five;

	Card secondCard;
	secondCard.suit = Diamonds;
	secondCard.number = Ten;

	PrintCard(firstCard);
	PrintCard(secondCard);

	cout << "Of the two cards, the higher is the where:\n";

	PrintCard(HighCard(firstCard, secondCard));
	
	_getch();
	return 0;
}
